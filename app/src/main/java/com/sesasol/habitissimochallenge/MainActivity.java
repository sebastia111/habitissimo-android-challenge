package com.sesasol.habitissimochallenge;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initButtons();
    }

    private void initButtons(){
        initListBudgetButton();
        initAddBudgetButton();
    }

    private void initListBudgetButton(){
        Button btn = (Button) findViewById(R.id.listBudgetButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListBudgetActivity.class));
            }
        });
    }

    private void initAddBudgetButton(){
        Button btn = (Button) findViewById(R.id.addBudget);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddBudgetActivity.class));
            }
        });
    }
}
