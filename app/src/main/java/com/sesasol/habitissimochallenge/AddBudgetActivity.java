package com.sesasol.habitissimochallenge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.sesasol.habitissimochallenge.adapters.MySpinnerAdapter;
import com.sesasol.habitissimochallenge.api.HabitissimoApi;
import com.sesasol.habitissimochallenge.models.Budget;
import com.sesasol.habitissimochallenge.models.Category;
import com.sesasol.habitissimochallenge.models.Location;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddBudgetActivity extends AppCompatActivity {

    private Spinner categorySpinner;
    private Spinner subCategorySpinner;
    private Spinner locationSpinner;
    private Spinner subLocationSpinner;
    private EditText nameTextInput;
    private EditText emailTextInput;
    private EditText phoneNumberTextInput;
    private EditText descriptionTextInput;

    private HabitissimoApi api;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);

        initApi();

        initInputFields();
        initSubmitButton();
        initSpinners();
    }

    private void initApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(HabitissimoApi.class);
    }

    private void initInputFields() {
        nameTextInput  = (EditText) findViewById(R.id.name);
        emailTextInput = (EditText) findViewById(R.id.email);
        phoneNumberTextInput = (EditText) findViewById(R.id.phone);
        descriptionTextInput = (EditText) findViewById(R.id.description);
    }

    private void initSubmitButton(){
        Button btn = (Button) findViewById(R.id.submitButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitClicked();
            }
        });
    }

    private void initSpinners() {
        initCategorySpinner();
        initLocationSpinner();
    }


    private void initCategorySpinner() {
        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);

        Call<List<Category>> categoryResponse;
        categoryResponse = api.listCategories();

        categoryResponse.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                MySpinnerAdapter<Category> adapter = new MySpinnerAdapter<>(
                        AddBudgetActivity.this,
                        android.R.layout.simple_spinner_item,
                        response.body());

                categorySpinner.setAdapter(adapter);
                setListenerCategorySpinner();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                shortToast(R.string.network_error);
            }
        });
    }
    
    private void setListenerCategorySpinner() {
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category)parent.getSelectedItem();
                String selectedCategoryId = selectedCategory.getId();

                fillSubCategorySpinner(selectedCategoryId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void fillSubLocationSpinner(int id) {
        subLocationSpinner = (Spinner) findViewById(R.id.subLocationSpinner);

        Call<List<Location>> locationResponse;
        locationResponse = api.listLocations(id);

        locationResponse.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {

                MySpinnerAdapter<Location> adapter = new MySpinnerAdapter<>(
                        AddBudgetActivity.this,
                        android.R.layout.simple_spinner_item,
                        response.body());

                subLocationSpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                shortToast(R.string.network_error);
            }
        });
    }

    private void setListenerLocationSpinner() {
        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Location selectedLocation = (Location)parent.getSelectedItem();
                int selectedLocationId = selectedLocation.getId();

                fillSubLocationSpinner(selectedLocationId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


    private void fillSubCategorySpinner(String id) {
        subCategorySpinner = (Spinner) findViewById(R.id.subCategorySpinner);

        Call<List<Category>> categoryResponse;
        categoryResponse = api.listSubcategories(id);

        categoryResponse.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                MySpinnerAdapter<Category> adapter = new MySpinnerAdapter<>(
                        AddBudgetActivity.this,
                        android.R.layout.simple_spinner_item,
                        response.body());

                subCategorySpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                shortToast(R.string.network_error);
            }
        });
    }

    private void initLocationSpinner() {
        locationSpinner = (Spinner) findViewById(R.id.locationSpinner);

        Call<List<Location>> locationResponse;
        locationResponse = api.listLocations();

        locationResponse.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {

                MySpinnerAdapter<Location> adapter = new MySpinnerAdapter<>(
                        AddBudgetActivity.this,
                        android.R.layout.simple_spinner_item,
                        response.body());

                locationSpinner.setAdapter(adapter);
                setListenerLocationSpinner();
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t){
                shortToast(R.string.network_error);
            }
        });
    }

    private void submitClicked() {
        Budget budget = createBudget();
        if(isValid(budget)) {
            budget.store(this);
            shortToast(R.string.entry_added);
        }else{
            shortToast(R.string.error_data_not_valid);
        }
    }

    private Budget createBudget() {
        String name, email, phone, description;
        Category category;
        Location location;

        name = nameTextInput.getText().toString();
        email = emailTextInput.getText().toString();
        phone = phoneNumberTextInput.getText().toString();
        description = descriptionTextInput.getText().toString();
        category = (Category)subCategorySpinner.getSelectedItem();
        location = (Location)subLocationSpinner.getSelectedItem();
        
        if (       name.isEmpty()
                || ! email.matches("\\w+(\\.\\w+)*@\\w+\\.\\w+") // email
                || ! phone.matches("\\d+") //Check if it's a number
                || category == null
                || description.isEmpty()
                || location == null){
            return null;
        }else{
            return new Budget(name, email, phone, category, description, location);
        }

    }

    private boolean isValid(Budget budget) {
        return !(budget==null);
    }

    private void shortToast(String message) {
        Toast.makeText(AddBudgetActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void shortToast(int resource) {
        Toast.makeText(AddBudgetActivity.this, resource, Toast.LENGTH_SHORT).show();
    }
}
