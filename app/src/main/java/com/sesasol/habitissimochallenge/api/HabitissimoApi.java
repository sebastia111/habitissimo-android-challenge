package com.sesasol.habitissimochallenge.api;

import com.sesasol.habitissimochallenge.models.Category;
import com.sesasol.habitissimochallenge.models.Location;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HabitissimoApi {
    String CATEGORY = "category/";
    String LOCATION = "location/";

    @GET(CATEGORY + "list")
    Call<List<Category>> listCategories();

    @GET(CATEGORY + "list/{categoryId}")
    Call<List<Category>> listSubcategories(@Path("categoryId") String categoryId);

    @GET(LOCATION + "list")
    Call<List<Location>> listLocations();

    @GET(LOCATION + "list/{parentId}")
    Call<List<Location>> listLocations(@Path("parentId") int parentId);

}
