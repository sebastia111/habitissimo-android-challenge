package com.sesasol.habitissimochallenge.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sesasol.habitissimochallenge.R;
import com.sesasol.habitissimochallenge.models.Budget;

import java.util.List;

/**
 * Created by sebass on 27/04/17.
 */

public class BudgetListAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Budget> budgets;

    public BudgetListAdapter(Context context, int layout, List<Budget> budgets) {
        this.context = context;
        this.layout  = layout;
        this.budgets   = budgets;
    }

    @Override
    public int getCount() {
        return this.budgets.size();
    }

    @Override
    public Object getItem(int position) {
        return this.budgets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(this.layout, null);

            holder = new ViewHolder();

            holder.nameTextView = (TextView) convertView.findViewById(R.id.textViewName);
            holder.categoryTextView = (TextView) convertView.findViewById(R.id.textViewCategory);
            holder.phonenumberTextView = (TextView) convertView.findViewById(R.id.textViewPhonenumber);
            holder.locationTextView = (TextView) convertView.findViewById(R.id.textViewLocation);
            holder.emailTextView = (TextView) convertView.findViewById(R.id.textViewEmail);
            holder.descriptionTextView = (TextView) convertView.findViewById(R.id.textViewDescription);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String currentName = budgets.get(position).getName();
        String currentCategory = budgets.get(position).getCategory().getName();
        String currentPhonenumber = budgets.get(position).getPhoneNumber();
        String currentLocation = budgets.get(position).getLocation().getName();
        String currentEmail = budgets.get(position).getEmail();
        String currentDescription = budgets.get(position).getDescription();

        holder.nameTextView.setText(currentName);
        holder.categoryTextView.setText(currentCategory);
        holder.phonenumberTextView.setText(currentPhonenumber);
        holder.locationTextView.setText(currentLocation);
        holder.emailTextView.setText(currentEmail);
        holder.descriptionTextView.setText(currentDescription);


        return convertView;
    }

    static class ViewHolder {
        private TextView nameTextView;
        private TextView categoryTextView;
        private TextView phonenumberTextView;
        private TextView locationTextView;
        private TextView emailTextView;
        private TextView descriptionTextView;

    }
}

