package com.sesasol.habitissimochallenge.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class MySpinnerAdapter<T> extends ArrayAdapter<T> {

    private Context context;
    private List<T> values;
    private int textViewResourceId;

    public MySpinnerAdapter(Context context, int textViewResourceId,
                            List<T> values) {
        super(context, textViewResourceId, values);
        this.textViewResourceId = textViewResourceId;
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }

    public T getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView v;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = (TextView) layoutInflater.inflate(this.textViewResourceId, null);

        v.setText(values.get(position).toString());
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        return this.getView(position, convertView, parent);
    }
}