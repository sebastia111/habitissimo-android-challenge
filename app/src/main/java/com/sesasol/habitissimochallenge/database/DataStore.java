package com.sesasol.habitissimochallenge.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sesasol.habitissimochallenge.models.Budget;
import com.sesasol.habitissimochallenge.models.Category;
import com.sesasol.habitissimochallenge.models.Location;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class DataStore extends SQLiteOpenHelper {

    public static final String BUDGET_TABLE_NAME = "budget";
    public static final String LOCATION_TABLE_NAME = "location";
    public static final String CATEGORY_TABLE_NAME = "category";

    public static final String COLUMN_ID = "_id";

    private static final String DATABASE_NAME = "budgets.db";
    private static final int DATABASE_VERSION = 1;

    private static final String BUDGET_DATABASE_CREATE = "create table "
            + BUDGET_TABLE_NAME + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + "name" + " text not null,"
            + "email" + " text not null,"
            + "phoneNumber" + " text not null,"
            + "category" + " text not null,"
            + "description" + " text not null,"
            + "location" + " integer"
            + ");";

    private static final String LOCATION_DATABASE_CREATE = "create table "
            + LOCATION_TABLE_NAME + "( "
            + COLUMN_ID + " integer primary key, "
            + "parentId" + " integer,"
            + "name" + " text not null,"
            + "zip" + " text not null,"
            + "geoLat" + " double,"
            + "level" + " integer,"
            + "geoLng" + " double,"
            + "slug" + " text not null"
            + ");";

    private static final String CATEGORY_DATABASE_CREATE = "create table "
            + CATEGORY_TABLE_NAME + "( "
            + COLUMN_ID + " text primary key, "
            + "parentId" + " text not null,"
            + "name" + " text not null,"
            + "slug" + " text not null"
            + ");";

    public DataStore(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(BUDGET_DATABASE_CREATE);
        database.execSQL(LOCATION_DATABASE_CREATE);
        database.execSQL(CATEGORY_DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DataStore.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + BUDGET_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE_NAME);
        onCreate(db);
    }

    public void addBudget(Budget budget) {
        addLocation(budget.getLocation()); //First we have to add the location to the DB
        addCategory(budget.getCategory());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("name", budget.getName());
        values.put("email", budget.getEmail());
        values.put("phoneNumber", budget.getPhoneNumber());
        values.put("category", budget.getCategory().getId());
        values.put("description", budget.getDescription());
        values.put("location", budget.getLocation().getId());

        db.insert(BUDGET_TABLE_NAME, null, values);

        db.close();
    }

    public List<Budget> getBudgets() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Budget> budgets = new ArrayList<>();
        String query = "SELECT * from " + BUDGET_TABLE_NAME + ";";
        Cursor result;

        result = db.rawQuery(query, null);

        for(result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
            String name = result.getString(1);
            String email = result.getString(2);
            String phoneNumber = result.getString(3);
            Category category = getCategory(result.getString(4));
            String description = result.getString(5);
            Location location = getLocation(result.getInt(6));

            Budget budget = new Budget(name, email, phoneNumber, category, description, location);
            budgets.add(budget);
        }

        return budgets;
    }

    public void addCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("_id", category.getId());
        values.put("parentId", category.getParentId());
        values.put("name", category.getName());
        values.put("slug", category.getSlug());

        try {
            db.insertOrThrow(CATEGORY_TABLE_NAME, null, values);
        }catch (SQLiteConstraintException e){
            Log.i("DataStore AddCategory", "Already exists, doing nothing");
        }
        db.close();
    }

    public Category getCategory(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Category category = null;
        String query = "SELECT * from " + CATEGORY_TABLE_NAME + " where _id = '"+ id +"';";
        Cursor result;
        result = db.rawQuery(query, null);

        result.moveToFirst();
        if(! result.isAfterLast()) {
            String _id = result.getString(0);
            String parentId = result.getString(1);
            String name = result.getString(2);
            String slug = result.getString(3);

            category = new Category(_id, parentId, name, slug);
        }

        return category;
    }


    public void addLocation(Location location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("_id", location.getId());
        values.put("parentId", location.getParentId());
        values.put("name", location.getName());
        values.put("zip", location.getZip());
        values.put("geoLat", location.getGeoLat());
        values.put("level", location.getLevel());
        values.put("geoLng", location.getGeoLng());
        values.put("slug", location.getSlug());

        try {
            db.insertOrThrow(LOCATION_TABLE_NAME, null, values);
        }catch (SQLiteConstraintException e){
            Log.i("DataStore AddLocation", "Already exists, doing nothing");
        }
        db.close();
    }

    public Location getLocation(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Location location = null;
        String query = "SELECT * from " + LOCATION_TABLE_NAME + " where _id = "+ id +";";
        Cursor result;
        result = db.rawQuery(query, null);

        result.moveToFirst();
        if(! result.isAfterLast()) {
            location = new Location(
                    result.getInt(0),
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getDouble(4),
                    result.getInt(5),
                    result.getDouble(6),
                    result.getString(7)
            );
        }

        return location;
    }

}
