package com.sesasol.habitissimochallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.sesasol.habitissimochallenge.adapters.BudgetListAdapter;
import com.sesasol.habitissimochallenge.database.DataStore;
import com.sesasol.habitissimochallenge.models.Budget;

import java.util.List;

public class ListBudgetActivity extends AppCompatActivity {

    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_budget);

        initButtons();
        initListView();
    }

    private void initButtons(){
        initAddBudgetButton();
    }

    private void initAddBudgetButton(){
        Button btn = (Button) findViewById(R.id.addBudget);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListBudgetActivity.this, AddBudgetActivity.class));
            }
        });
    }

    private void initListView() {
        List<Budget> budgets;
        DataStore db = new DataStore(this);
        budgets = db.getBudgets();

        listView = (ListView) findViewById(R.id.budgetsListView);

        BudgetListAdapter myAdapter = new BudgetListAdapter(this, R.layout.budget_list_item, budgets);
        listView.setAdapter(myAdapter);

    }
    
}
