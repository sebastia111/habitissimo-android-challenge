package com.sesasol.habitissimochallenge.models;

import android.content.Context;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sesasol.habitissimochallenge.database.DataStore;

import java.util.List;

/**
 * Created by sebass on 23/04/17.
 */

public class Budget {

    private String name;
    private String email;
    private String description;
    private String phoneNumber;
    private Category category;
    private Location location;

    public Budget(String name, String email, String phoneNumber, Category category,
                  String description, Location location){

        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.category = category;
        this.description = description;
        this.location = location;
    }

    /*
    public Budget(String name, String email, String phoneNumber, String category,
                  String description, Location location){

        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.category = Integer.parseInt(category.replaceFirst("002\\-", ""));
        this.description = description;
        this.location = location;
    }
    */


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void store(Context context) {
        DataStore db = new DataStore(context);
        db.addBudget(this);
    }

}
