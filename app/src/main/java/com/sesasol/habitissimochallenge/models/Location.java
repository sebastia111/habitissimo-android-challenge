package com.sesasol.habitissimochallenge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.ref.SoftReference;

/**
 * Created by sebass on 21/04/17.
 */


public class Location {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("geo_lat")
    @Expose
    private Double geoLat;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("geo_lng")
    @Expose
    private Double geoLng;
    @SerializedName("slug")
    @Expose
    private String slug;

    public Location(){

    }

    public Location(int id, int parentId, String name, String zip, Double geoLat,
                    int level, Double geoLng, String slug) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.zip = zip;
        this.geoLat = geoLat;
        this.level = level;
        this.geoLng = geoLng;
        this.slug = slug;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Double getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(Double geoLat) {
        this.geoLat = geoLat;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getGeoLng() {
        return geoLng;
    }

    public void setGeoLng(Double geoLng) {
        this.geoLng = geoLng;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
